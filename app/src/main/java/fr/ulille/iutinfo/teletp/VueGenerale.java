package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    public String DISTANCIEL;
    public String poste;
    public String salle;

    private SuiviViewModel suiviViewModel;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState

    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }


    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.DISTANCIEL  = getResources().getStringArray(R.array.list_salles)[0];
        this.poste = "";
        this.salle = DISTANCIEL;
        suiviViewModel = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);
        Spinner spSalle = view.findViewById(R.id.spSalle);
        Spinner spPoste = view.findViewById(R.id.spPoste);
        ArrayAdapter<CharSequence> adaSalle = ArrayAdapter.createFromResource(this.getContext(),R.array.list_salles,R.layout.support_simple_spinner_dropdown_item);
        adaSalle.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        ArrayAdapter<CharSequence> adaPoste = ArrayAdapter.createFromResource(this.getContext(),R.array.list_postes,R.layout.support_simple_spinner_dropdown_item);
        adaPoste.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spSalle.setAdapter(adaSalle);
        spPoste.setAdapter(adaPoste);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            EditText login = view.findViewById(R.id.tvLogin);
            suiviViewModel.setUsername(login.getText().toString());
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });
        update();
        spSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                salle = adapterView.getItemAtPosition(i).toString();
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spPoste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                poste = adapterView.getItemAtPosition(i).toString();
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        // TODO Q9
    }

    public void update(){
        Log.d("Select","item "+salle+" selected");
        Spinner spSalle = getActivity().findViewById(R.id.spSalle);
        Spinner spPoste = getActivity().findViewById(R.id.spPoste);
        if(salle.equals(DISTANCIEL)){
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
        }else{
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
            String salle = spSalle.getSelectedItem().toString();
            String poste = spPoste.getSelectedItem().toString();
            String[] tab = salle.split("(|)");
            suiviViewModel.setLocalisation("" + tab[0].toUpperCase()+": "+tab[1].toUpperCase()+poste);
        }
    }
    // TODO Q9
}